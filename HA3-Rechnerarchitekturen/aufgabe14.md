# Aufgabe 4.1 (a)

1. **Kollisionen und Wartezeiten**: Wenn sowohl Daten als auch Befehle denselben Bus verwenden, können Kollisionen auftreten, was zu Wartezeiten führt, da der Bus immer nur von einer Einheit zur gleichen Zeit verwendet werden kann.

2. **Performance-Probleme**: Der gemeinsame Bus kann zu einem Engpass werden, wenn viele Zugriffe auf Daten und Befehle gleichzeitig stattfinden. Dies kann die Gesamtleistung des Systems beeinträchtigen, da die CPU auf den Zugriff auf den Bus warten muss.

3. **Komplexität des Designs**: Das Design von Steuerlogik wird komplexer, da Mechanismen zur Verwaltung und Priorisierung der Zugriffe auf den Bus erforderlich sind. Dies erhöht die Komplexität und die Kosten der Hardware.

4. **Skalierbarkeit**: In Systemen, die viele Peripheriegeräte oder Prozessoren verwenden, wird die gemeinsame Nutzung eines Busses schnell unpraktisch, da die Anforderungen an die Busbandbreite steigen. Dies kann zu einem Flaschenhals führen, der die Systemleistung drastisch reduziert.

5. **Sicherheitsrisiken**: Die gemeinsame Nutzung eines Busses kann Sicherheitsrisiken erhöhen, da Daten und Befehle potenziell von nicht autorisierten Einheiten abgefangen oder manipuliert werden können.

Durch die Trennung von Daten- und Befehlsbussen können diese Nachteile minimiert werden. Dies führt zu einer besseren Systemleistung und erhöht die Effizienz und Sicherheit des Datenverarbeitungsprozesses.

# Aufgabe 4.1 (b)

**Jumps if Zero (JZ) / Jump if Equal (JE)**:
   - Springt zu einer bestimmten Adresse, wenn das Ergebnis einer vorherigen Operation null oder einem bestimmten Wert entspricht.

**Jumps if Not Zero (JNZ) / Jump if Not Equal (JNE)**:
   - Springt zu einer bestimmten Adresse, wenn das Ergebnis einer vorherigen Operation nicht null oder einem bestimmten Wert entspricht.

**Jumps if Greater (JG) / Jump if Greater or Equal (JGE)**:
   - Springt zu einer bestimmten Adresse, wenn das Ergebnis einer vorherigen Operation größer oder gleich einem bestimmten Wert ist.

**Jumps if Less (JL) / Jump if Less or Equal (JLE)**:
   - Springt zu einer bestimmten Adresse, wenn das Ergebnis einer vorherigen Operation kleiner oder gleich einem bestimmten Wert ist.

### Unbedingtes Springen:
**Jump (JMP)**:
   - Springt immer zu einer angegebenen Adresse, unabhängig von vorherigen Operationsergebnissen.

### Kontrollstrukturen, die damit umgesetzt werden können:

**Verzweigungen (if-else)**:
   - Durch bedingte Sprünge wie JZ, JNZ, JG, JL können Verzweigungen implementiert werden. Zum Beispiel:
     ```
     CMP AX, BX        ; Vergleiche AX und BX
     JZ  label_equal   ; Springe zu label_equal, wenn AX gleich BX ist
     JMP end_label     ; Springe am Ende der bedingten Verzweigung
     label_equal:      ; Code, der ausgeführt wird, wenn AX und BX gleich sind
                       ; dann hier der Code, der ausgeführt wird, wenn AX und BX gleich sind
     end_label:        ; Code, der nach der bedingten Verzweigung ausgeführt wird
                       ; dann hier der Code, der nach der bedingten Verzweigung ausgeführt wird
     ```

**Schleifen (for, while)**:
   - Schleifen können durch unbedingte Sprünge (JMP) und bedingte Sprünge (JZ, JNZ) realisiert werden. Zum Beispiel:
     ```
     MOV CX, 10      ; Initialisierung der Schleifenvariable
     loop_start:
     CMP CX, 0       ; Bedingung prüfen
     JZ loop_end     ; Schleife beenden, wenn CX gleich null ist (Code innerhalb der Schleife)
                     ; dann hier der Code, der innerhalb der Schleife ausgeführt wird
     DEC CX          ; Schleifenvariable dekrementieren
     JMP loop_start  ; Springe zum Schleifenanfang
     loop_end:
                     ; dann hier der Code, der nach der Schleife ausgeführt wird
     ```

**Fallunterscheidungen (switch-case)**:
   - Obwohl Maschinencode keine eingebauten switch-case-Anweisungen hat, können mehrere bedingte Sprünge verwendet werden, um ähnliche Logik zu erreichen. Zum Beispiel:
```
CMP AX, 1        ; Vergleiche AX mit 1
JE case_1        ; Springe zu case_1, wenn AX gleich 1 ist
CMP AX, 2        ; Vergleiche AX mit 2
JE case_2        ; Springe zu case_2, wenn AX gleich 2 ist
JMP default_case ; Springe zum Standardfall, wenn keine der vorherigen Bedingungen erfüllt ist

case_1:
                 ; Code für Fall 1
JMP end_switch   ; Springe zum Ende der switch-case Struktur

case_2:
                 ; Code für Fall 2
JMP end_switch   ; Springe zum Ende der switch-case Struktur

default_case:
                 ; Code für den Standardfall

end_switch:
                 ; Hier wird der Code fortgesetzt, der nach der switch-case Struktur ausgeführt werden soll
```
