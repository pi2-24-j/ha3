# Aufgabe (a)

### Vier verschiedene Zustände, die ein Prozess in einem Betriebssystem haben kann:

1. **Laufend (Running)**:
   - Der Prozess befindet sich in Ausführung auf der CPU und führt tatsächlich Anweisungen aus.

2. **Bereit (Ready)**:
   - Der Prozess ist bereit, auf der CPU ausgeführt zu werden, wartet jedoch darauf, dass ihm Prozessorzeit zugewiesen wird.

3. **Blockiert (Blocked oder Waiting)**:
   - Der Prozess kann nicht weiterarbeiten, bis ein bestimmtes Ereignis eintritt (z.B. Eingabe/Ausgabe-Operation abgeschlossen, Semaphore freigegeben).

4. **Beendet (Terminated)**:
   - Der Prozess hat seine Ausführung abgeschlossen und alle zugehörigen Ressourcen wurden freigegeben. Er wurde aus dem Speicher des Betriebssystems entfernt.

Diese Zustände sind grundlegend für das Management von Prozessen in einem Betriebssystem und helfen dabei, den Lebenszyklus und die Interaktionen der Prozesse im System zu verstehen.

# Aufgabe (b)

### Unterschied zwischen User Mode und Kernel Mode:

**User Mode:**
- Auch bekannt als Benutzermodus
- In diesem Modus laufen normale Anwendungsprogramme
- Programme im User Mode haben begrenzten Zugriff auf Systemressourcen und -funktionen
- Zugriff auf bestimmte kritische Ressourcen und Funktionen des Betriebssystems ist eingeschränkt oder nicht möglich

**Kernel Mode:**
- Auch bekannt als privilegierter Modus oder Systemmodus
- Der Kernel (Kern des Betriebssystems) läuft im Kernel Mode
- Hat vollen Zugriff auf alle Ressourcen und Funktionen des Systems
- Kann privilegierte Anweisungen ausführen und direkt mit Hardware kommunizieren
- Ist erforderlich, um Aufgaben wie Verwaltung von Speicher, Prozessen, Geräten und Dateisystemen durchzuführen

**Unterschied:**
- Der Hauptunterschied liegt im Zugriffslevel auf Systemressourcen und -funktionen
- Im User Mode sind Programme isoliert und haben eingeschränkten Zugriff, während der Kernel Mode uneingeschränkten Zugriff und Kontrolle über das gesamte System bietet
- Der Übergang zwischen User Mode und Kernel Mode erfordert spezielle Mechanismen wie Systemaufrufe (Syscalls) für Programme, um auf Kernel-Funktionen zuzugreifen, wobei der Kontextwechsel sorgfältig kontrolliert wird, um Sicherheit und Stabilität zu gewährleisten

# Aufgabe (c)

### Befehl zur Anzeige der derzeit vom Benutzer ausgeführten Prozesse:

Der Befehl `ps` kann verwendet werden, um die derzeit vom Benutzer ausgeführten Prozesse anzuzeigen.

```bash
ps ux
```


# Aufgabe (d)

### UNIX-Befehl zur Erteilung von Berechtigungen zur Ausführung einer Datei

Um Berechtigungen zur Ausführung einer Datei in Unix/Linux zu erteilen, verwenden Sie den Befehl `chmod` (change mode). Hier wird sowohl die symbolische als auch die oktale Form des Befehls erklärt:

**Symbolische Form:**
```bash
chmod +x mvnw
```

**Oktale Form**
```bash
chmod 755 mvnw
```

Die 7, steht für den Benutzer/Besitzer und die erste 5 für die Gruppe (Lese- und Ausführungsberechtigungen), wobei die zweite 5 für alle Anderen (öffentliche Berechtigungen) steht.
